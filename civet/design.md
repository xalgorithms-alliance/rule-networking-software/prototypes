# Queue directories

- `in.ior`   => `submit/documents`
- `rule.ior` => `submit/rules`
- `rule.ior` <= `results/rules/<in.ior>`

Content ids (rule will be pulled from IPFS) can be submitted to
`submit/content_ids` and `civet` will pull them from IPFS.

Multiple documents can be submitted to `submit/documents`. When something is submitted to `submit/sieve`, they're processed.
