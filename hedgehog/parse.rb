require 'ostruct'

class Val
  MATCHES = {
    /^'(.+)'$/               => ->(vs) { String(vs) },
    /^([0-9]+)$/             => ->(vs) { Integer(vs) },
    /^([0-9](?:\.[0-9]+)?)$/ => ->(vs) { Float(vs) },
  }

  def self.parse(s)
    MATCHES.keys.each do |re|
      m = re.match(s)
      return MATCHES[re].(m[1]) if m
    end
  end
end

class Expr < OpenStruct; end
class Equals < Expr; end
class Invalid < Expr; end

class Expr
  OPS = {
    '=' => Equals,
  }

  def self.parse(s)
    m = /([a-zA-Z][a-zA-Z0-9_]*)([\=])(.+)/.match(s)
    if m
      (k, op, v) = m[1..3]
      return OPS.fetch(op, Invalid).new(key: k, val: Val.parse(v))
    end
  end

  def eval(v)
    false
  end
end

class Parser
  attr_reader :conditions
  attr_reader :assertions

  def self.parse_file(fn)
    parser = Parser.new

    File.readlines(fn).each do |ln|
      parser.accept(ln)
    end

    [parser.conditions, parser.assertions]
  end

  def initialize
    @state = method(:state_default)
    @conditions = []
    @assertions = []
  end

  def accept(ln)
    return if comment?(ln)

    @state = @state.call(ln)
  end

  private

  def next_state(ln)
    case ln.chomp
    when 'CONDITIONS'
      method(:state_conditions)
    when 'ASSERTIONS'
      method(:state_assertions)
    else
      @state
    end
  end

  def comment?(ln)
    ln =~ /\s*\#.*/
  end

  def state_default(ln)
    next_state(ln)
  end

  def state_conditions(ln)
    m = /^\s*(.+)\:\s*\[(.+)\]\s*$/.match(ln)
    if m
      (expr, scenarios) = m[1..2]
      @conditions << OpenStruct.new(
        expr: Expr.parse(expr),
        scenarios: scenarios.split(/\s*\,\s*/),
      )
    end
    state_default(ln)
  end

  def state_assertions(ln)
    m = /^\s*(.+)\:\s*(.+)\s*\:\s*\[(.+)\]\s*$/.match(ln)
    if m
      (k, v, scenarios) = m[1..3]
      @assertions << OpenStruct.new(
        key: k,
        val: Val.parse(v),
        scenarios: scenarios.split(/\s*\,\s*/),
      )
    end
    state_default(ln)
  end
end
