require('active_support')
require('active_support/core_ext/hash')

require_relative('../../lib/triad')
require_relative('./rule_gen')

RSpec.describe(Triad::Rules::Rule) do
  include RuleGen

  it 'should parse a rule from JSON' do
    rule = generate_rule
    h = rule.to_h.with_indifferent_access
    ac_h = Triad::Rules::Rule.from_h(h).to_h.with_indifferent_access
    expect(ac_h).to eql(h)
  end

  it 'should accept in_effect with Time OR date strings' do
    (in_effect, conds, asserts) = generate_rule_parts

    rule = Triad::Rules::Rule.new(in_effect, conds, asserts, { id: Faker::Internet.uuid })
    in_effect.zip(rule.in_effect).each do |(ex, ac)|
      expect(ac[:from]).to eql(ex[:from])
      expect(ac[:to]).to eql(ex[:to])
    end

    in_effect_s = in_effect.map do |tup|
      tup.merge(to: tup[:to].iso8601, from: tup[:from].iso8601)
    end

    rule = Triad::Rules::Rule.new(in_effect_s, conds, asserts, { id: Faker::Internet.uuid })
    in_effect.zip(rule.in_effect).each do |(ex, ac)|
      expect(ac[:from]).to eql(ex[:from])
      expect(ac[:to]).to eql(ex[:to])
    end
  end

  it 'should accept JSON with Time OR date strings' do
    (in_effect, conds, asserts) = generate_rule_parts

    rule = Triad::Rules::Rule.from_h(
      {
        'in_effect'         => in_effect,
        'input_conditions'  => conds.map(&:to_h),
        'output_assertions' => asserts.map(&:to_h),
        'properties'        => { id: Faker::Internet.uuid }
      }.with_indifferent_access
    )

    in_effect.zip(rule.in_effect).each do |(ex, ac)|
      expect(ac[:from]).to eql(ex[:from])
      expect(ac[:to]).to eql(ex[:to])
    end

    in_effect_s = in_effect.map do |tup|
      tup.merge(to: tup[:to].iso8601, from: tup[:from].iso8601)
    end

    rule = Triad::Rules::Rule.from_h(
      {
        'in_effect'         => in_effect,
        'input_conditions'  => conds.map(&:to_h),
        'output_assertions' => asserts.map(&:to_h),
        'properties'        => { id: Faker::Internet.uuid }
      }.with_indifferent_access
    )

    in_effect.zip(rule.in_effect).each do |(ex, ac)|
      expect(ac[:from]).to eql(ex[:from])
      expect(ac[:to]).to eql(ex[:to])
    end
  end

  it 'should yield JSON with ISO8601 strings' do
    (in_effect, conds, asserts) = generate_rule_parts

    rule = Triad::Rules::Rule.new(in_effect, conds, asserts, { id: Faker::Internet.uuid })
    in_effect.zip(rule.to_h[:in_effect]).each do |(ex, ac)|
      expect(ac[:from]).to eql(ex[:from].iso8601)
      expect(ac[:to]).to eql(ex[:to].iso8601)
    end
  end
end
