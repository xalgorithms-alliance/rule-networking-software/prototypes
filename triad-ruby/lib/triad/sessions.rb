require('fileutils')
require('json')
require('securerandom')

module Triad
  module Sessions
    ROOT_PATH = 'sessions'

    def self.list(root_path=ROOT_PATH)
      Dir[File.join(root_path, '*')].map do |sess_dir|
        Session.new(root_path, File.basename(sess_dir))
      end
    end

    def self.load(id, root_path=ROOT_PATH)
      Session.new(root_path, id)
    end

    class Copy
      attr_reader(:time)
      attr_reader(:content)

      def initialize(t, c)
        @time = t
        @content = c
      end
    end

    class Rule
      attr_reader(:id)
      attr_reader(:path)

      def self.make(root_path)
        r = Rule.new(root_path, SecureRandom.uuid)
        r.store
        r
      end

      def initialize(root_path, id)
        @id = id
        @path = File.join(root_path, id)
      end

      def copies
        Dir[File.join(@path, '*')].map do |cp_path|
          ts = File.basename(cp_path, '.json').to_i
          Copy.new(Time.at(ts), JSON.parse(File.read(cp_path)))
        end
      end

      def store
        FileUtils.mkdir_p(@path)
      end

      def add(content)
        store

        copy_id = Time.now.to_i
        while File.exist?(File.join(@path, "#{copy_id}.json"))
          copy_id = copy_id + 1
        end
        File.write(File.join(@path, "#{copy_id}.json"), content.to_json)
        copy_id
      end
    end

    class Session
      attr_reader(:id)
      attr_reader(:path)

      def self.make(root_path=ROOT_PATH)
        sess = Session.new(root_path, SecureRandom.uuid)
        sess.store
        sess
      end

      def initialize(root_path, id)
        @id = id
        @path = File.join(root_path, @id)
      end

      def store
        FileUtils.mkdir_p(@path)
      end

      def rules
        @rules ||= Dir[File.join(@path, '*')].map do |path|
          Rule.new(File.dirname(path), File.basename(path))
        end
      end
    end
  end
end
